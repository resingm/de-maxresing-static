# nginx base image

FROM nginx

# Remove default configuration
RUN rm /etc/nginx/conf.d/default.conf

# Copy configuration
COPY nginx.conf /etc/nginx/conf.d/default.conf

# Copy static content
COPY dist /usr/share/nginx/html


#!/bin/bash

# Ensure to run as the intended user:
# runuser -l $USER -c '<command>'


declare -a SRC=( ./content/* ./html/* )
deploy_msg="automatic deployment - generated new ToC"
dist=./dist
deploy_dir="/var/www/static.maxresing.de/html/"

MINIFIER="https://cssminifier.com/raw"

#
# Pull latest changes
#
echo "Pulling latest changes..."
git pull
echo "Updated repository."


#
# Generating new ToC
#
./toc.py content html/content.html

#
# Create dist
#
rm -R ${dist}
mkdir -p -v ${dist}
for f in ${SRC[@]}
do
    cp -r $f ${dist}
done


#
# Minify CSS
#
for f in ${dist}/css/*.css; do
  filename=$(basename $f ".css");
  wget --post-data="input=`cat ${f}`" --output-document="${dist}/css/${filename}.min.css" https://cssminifier.com/raw
done


echo "Sucessfully prepared dist folder."
echo ""

echo "Remove existing files."
ssh remote.maxresing.de rm -rf ${deploy_dir}

echo "Copying files to remote destination"
scp -r ${dist}/* remote.maxresing.de:${deploy_dir}

echo "Changing ownership."
ssh remote.maxresing.de chown -R max:caddy ${deploy_dir}

echo "Finished deployment."
exit 0


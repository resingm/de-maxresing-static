#!/usr/bin/env python3

import sys

from datetime import datetime
from os import walk
from pystache import render

if len(sys.argv) not in [3, 4]:
    print("""
{0} - Generates a ToC (Table of content) of the files in the FROM directory.
      The result is stored in the TO file. A mustache template is required. 

Usage:
    {0} [template] FROM TO
    {0} FROM TO

Options:
    [template]            Requires a template file, by default 'toc.template'

    """.format(sys.argv[0]))

    exit(1);

TEMPLATE = ("toc.template" if len(sys.argv) == 3 else sys.argv[-3])
FROM = sys.argv[-2]
TO = sys.argv[-1] 

if not FROM[-1] == '/':
    FROM += '/'

BLACKLIST = [
]

GREY_LIST = [
        "doc/",
        "img/",
]

data = {
    "white" : [],
    "grey" : [{"name" : e} for e in GREY_LIST],
    "date" : datetime.utcnow().isoformat(timespec='seconds'),
}

for path, subdirs, files in walk(FROM):
    #print(str(root) + str(dirs) + str(files))
    for f in files:
        fname = path[len(FROM):] + "/" + f;

        if fname in BLACKLIST:
            continue

        if fname.startswith(tuple(GREY_LIST)):
            continue

        data["white"].append({
            "name" : fname
        })

#sort data entries
for e in ["white", "grey"]:
    data[e] = sorted(data[e], key=lambda k: k['name'])

with open(TEMPLATE, "r") as f:
    html = render(f.read(), data)
    
    with open(TO, "w") as d:
        d.write(html)



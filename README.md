# static.maxresing.de

Git repository of my static file hosting.
Feel free to use the hosted files for your personal projects.


## Contributions

If you are interested in contributing to the file diversity don't hesitate to contact me.
Generally, you have to agree that the files on this service are published under the same license as the files already online.
The deployed files will also appear on this [Git repository](https://codeberg.org/rem/de-maxresing-static).


## Feedback

Feedback is welcomed. Check out [About](https://static.maxresing.de/about.html) or [maxresing.de/contact.html](https://www.maxresing.de/contact.html) for contact details.


## TODO

* Improved ToC generation
* Auto deploy
